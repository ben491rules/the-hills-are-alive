#!/usr/bin/env python
# coding: utf-8

# In[16]:


import arcpy

import matplotlib.pyplot as plot


# In[17]:


dem = arcpy.GetParameter(0)
az = arcpy.GetParameter(1)
alt = arcpy.GetParameter(2)
shadow = arcpy.GetParameter(3)
z = arcpy.GetParameter(4)
out = arcpy.GetParameterAsText(5)


# In[15]:


outraster = arcpy.sa.Hillshade(in_raster=dem, azimuth=az, altitude=alt, model_shadows=shadow, z_factor=z)

out_raster.save(out)
# In[ ]:




