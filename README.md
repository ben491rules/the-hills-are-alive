```python
#The Hills Are Alive
```


```python
Thank you for choosing my custom tool set, The Hills Are Alive.  I've found that most of what I do in GIS (so far) has been creating data about the topography of an area of study.  While I love doing this process over an over again (and let it be known, this isn't sarcasm), I know that some people may not enjoy doing all these processes repeatedly for any given area.  I have devised a way to streamline the process so that with a few simple inputs, you can create a suite of topographic data in much less time than usual.  These data sets can then be used in many differet applications, so get creative!
```


      File "<ipython-input-2-b52be9d7743a>", line 1
        Thank you for choosing my custom tool set, The Hills Are Alive.  I've found that most of what I do in GIS (so far) has been creating data about the topography of an area of study.  While I love doing this process over an over again (and let it be known, this isn't sarcasm), I know that some people may not enjoy doing all these processes repeatedly for any given area.  I have devised a way to streamline the process so that with a few simple inputs, you can create a suite of topographic data in much less time than usual.
                ^
    SyntaxError: invalid syntax
    



```python
There are only a few steps to get this tool to work.  

First, select one or more DEM rasters.  I say one or more, since with this suite of tools, any DEM's you enter at first will be made into a mosaic!

Second, you will choose the location that the mosaic will be stored. 

Third, choose the name that it will be saved under.  

Fourth, choose the number of bands for the mosaic.  As you can see, many of the inputs of the original mosaic tool have been removed to streamline the process.

The tool will take care of a lot of the processes that would normally be up to you to either add, or more likely, ignore since it's optional.

But before you think you're done, there's more to do.

Fifth, you'll need to select an interval for the contour lines being created.

Last, select the output name and location for the contour lines.

That's it.  Nothing more.  After this, the tool takes care of the work of giving you a moderate pile of data for terrain analysis.  I hope that this helps you get more done faster.
```
