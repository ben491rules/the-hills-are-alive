#!/usr/bin/env python
# coding: utf-8

# In[18]:


import arcpy
import os


# In[19]:


dem = arcpy.GetParameterAsText(0)
outputplace = arcpy.GetParameterAsText(1)
outname = arcpy.GetParameterAsText(2)
band = arcpy.GetParameter(3)
intr = arcpy.GetParameter(4)
outcontour = arcpy.GetParameterAsText(5)

arcpy.management.MosaicToNewRaster(input_rasters=dem,
                                   output_location=outputplace,
                                   raster_dataset_name_with_extension=outname,
                                   number_of_bands=band)


# In[20]:


arcpy.sa.Hillshade(in_raster=outname)


# In[23]:


arcpy.sa.Contour(in_raster=outname, 
        out_polyline_features=outcontour, 
        contour_interval=intr)


# In[25]:


arcpy.sa.Slope(in_raster=outname)


# In[ ]:




